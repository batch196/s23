console.log('hello');

//JS objects - is a way to define a real world object with its own characteristics
//It is also a way to organize data with context through the use of key-vlaue pairs

/*
loyal,
huggable,
furry,
hyperactive,
tail,
breed,

*/
//if you want to decribe ,use object
let dog = {

	breed: 'Husky',
	color: 'Gray',
	legs: 4,
	isHuggable: true,
	isLoyal: true

}

//mini activity

let favoriteMovie = {

	title: 'Jackass: The Movie',
	publisher: 'MTV',
	year: 2022,
	director: 'Jeff Tremaine',
	isAvailable: true
}
console.log(favoriteMovie);
//{} are object literals
//objects are composed of key-value pairs. keys provide label to your value
// key : value
//each key-value pair tigether are called properties separated by comma

//accessing aray items = arrName[index]
// Accessing object property = objectName.propertyName

console.log(favoriteMovie.title);

console.log(favoriteMovie.publisher);

//update the property of object
favoriteMovie.year = 2050;
console.log(favoriteMovie);

//mini acitivity

favoriteMovie.title = "Ang Probinsyano"
favoriteMovie.publisher = "FPJ Production"
favoriteMovie.year = "2001";
console.log(favoriteMovie);

//objects can not only have primitive values like strings, numbers pr boolean
//it can be objects and arrays

let course ={

	title: 'Philosophy 101',
	description: 'Learn the values of life',
	price: 5000,
	isActive: true,
	instructors: ['Mr. Johnson','Mrs. Smith','Mr.Francis']

};
console.log(course);

//access the instructors array
console.log(course.instructors);

//access 'Mrs. Smith'
console.log(course.instructors[1]);

//use the instructor array method
console.log(course.instructors.pop());
console.log(course);

//mini activity

console.log(course.instructors.push('Mr. Mc Gee'));
console.log(course.instructors);
let output = course.instructors.includes('Mr. Johnson');
console.log(output);

//create a function to be able to add instructor
//add if-else statement which will determine if the added is already in the array
//


function addNewInstructor(instructor){

	course.instructors.push(instructor);
}
addNewInstructor('Mr. Marco');
addNewInstructor('Mr. Smith');

let instructor = addNewInstructor;//butang2x ra
let isAdded = course.instructors.includes(instructor);

if(isAdded){
	console.log('already added');
} else {
	course.instructors.push(instructor);
	console.log('Thank you');
}
console.log(course.instructors);

addNewInstructor('Mr. Smith');

// let instructor = {};
// console.log(instructor);

//create objects using a constructor function

//create a reusable function to create object whose structure and keys are the same. Think of a function that serves as blueprint for an object

function SuperHero(name,superpower,powerlevel){
	//this keyword when added in a constructor function refers to the object that will be made by the funcion
	this.name = name;
	this.superpower = superpower;
	this.powerlevel = powerlevel;
}

//create an object out of Suoerhero constructor

let superhero1 = new SuperHero('Saitama','One Punch', 30000);
console.log(superhero1);


//mini activity

function Laptop(name,brand,price){
	this.name = name;
	this.brand = brand;
	this.price = price;
}
let laptop1 = new Laptop('MacBook','Mac', 90000);
let laptop2 = new Laptop('AcerExample', 'Acer', 30000);
let laptop3 = new Laptop('LenovoExample', 'Lenovo',50000);
console.log(laptop1);
console.log(laptop2);
console.log(laptop3);

//object method are functions that are associated with an object
//a function is a property of an object and that function belongs to the object
//method are tasks that an object can perform or do

let person = {
	name: 'Slim Shady',
	talk: function(){
		//methods are functions associated as a property of an object
		//they are anonymous
		//this refers to the object where the method is associated
		// console.log(this);
		console.log('Hi my name is what? my name is who? my name is ' + this.name);
	}
}


let person2 = {
	name: 'Dr. Dre',
	greet: function(friend){
		//greet() method should be able to recieve a single object
		console.log('good day ' + friend.name);
	}
}
person.talk();

person2.greet(person);

//create a constructor with a built-i method

function Dog(name,breed){

	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log('Bark! Bark! ' + friend.name);
	}

}
let dog1 = new Dog('Max','Labrador');
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog('Sky','Husky');
console.log(dog2);














