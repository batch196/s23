console.log('hello');

let trainer = {

	name   : 'Sky',
	age    : 18,
	pokemon: ['Pikachu','Bulbasaur','Charmainder'],
	friends: new Array('Max','Maddy','Marcus','Brit'),
	talk   : function(){
		console.log('Pikachu! I choose you!');
	},
}

console.log('result of dot notation');
console.log(trainer.name);

console.log('result of square bracket notation');
console.log(trainer['pokemon']);
trainer.talk();


//------------------------------------------

function CreatePokemon(name,level){
	this.name   = name;
	this.level  = level;
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(attackedPokemon){
		console.log('This pokemon ' + this.name + ' tackled ' + attackedPokemon.name);
		attackedPokemon.health = attackedPokemon.health -10;
		console.log(attackedPokemon.name + ' has reduced to health to ' + attackedPokemon.health);
	}
	this.faint	= function(){
		console.log('This pokemon ' + this.name + ' has fainted');
	}
}

let pokemon1 = new CreatePokemon('Pikachu',12);

let pokemon2 = new CreatePokemon(
	'Bulbasaur',
	8
);
let pokemon3 = new CreatePokemon('Charmainder',15);
let pokemon4 = new CreatePokemon('Meow',18);


pokemon1.tackle(pokemon3);
pokemon3.faint();











